package eu.espeo.workshops.cr.stats;

import static eu.espeo.workshops.cr.repository.Specification.all;
import static java.util.UUID.randomUUID;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import lombok.RequiredArgsConstructor;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Named
@Path("/stats")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class StatsResources {

    private final StatsRepository statsRepository;

    @GET
    public Collection<Stats> list() {
        return statsRepository.query(all());
    }

    @POST
    public Stats create(Stats stats) {
        if (stats.getId() == null) {
            stats = stats.withReviewId(randomUUID());
        }
        statsRepository.add(stats);
        return stats;
    }
}
