package eu.espeo.workshops.cr.util;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

import org.springframework.context.annotation.Scope;

import java.util.Random;

import javax.inject.Named;

@Named
@Scope(SCOPE_PROTOTYPE)
class DefaultRandomNumberGenerator implements RandomNumberGenerator {

    private final Random random = new Random();

    @Override
    public int randomInt(int max) {
        return random.nextInt(max);
    }
}
