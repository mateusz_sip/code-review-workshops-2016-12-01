package eu.espeo.workshops.cr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeReviewStatisticsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeReviewStatisticsApplication.class, args);
    }
}
