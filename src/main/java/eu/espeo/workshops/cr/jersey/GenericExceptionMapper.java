package eu.espeo.workshops.cr.jersey;

import static eu.espeo.workshops.cr.jersey.Error.errorMessage;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.serverError;
import static javax.ws.rs.core.Response.status;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Slf4j
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable t) {
        log.error("Unexpected exception", t);
        return responseBuilder(t)
                .type(APPLICATION_JSON_TYPE)
                .build();
    }

    private static ResponseBuilder responseBuilder(Throwable t) {
        if (t instanceof WebApplicationException) {
            return webError((WebApplicationException) t);
        } else {
            return defaultError(t);
        }
    }

    private static ResponseBuilder webError(WebApplicationException t) {
        Response response = t.getResponse();
        return status(response.getStatus())
                .entity(errorMessage(response.getStatusInfo().getReasonPhrase()));
    }

    private static ResponseBuilder defaultError(Throwable t) {
        return serverError()
                .entity(errorMessage("Unexpected exception: " + t.getMessage()));
    }
}
